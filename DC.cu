#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "cuda.h"
#include <omp.h>

#define EPS 1e-10
#define pi 3.141592653589793238462643
#define n 4


__device__ void Givens(double* x, double *y, double *c, double *s){

	double xx = *x; 
	double yy = *y;
	double cc = *c;
	double ss = *s;

	if (fabs(yy)<EPS)
	{
   	 cc = 1.0;
   	 ss = 0.0;
	}
	else if (fabs(xx)<EPS)
	{
   	 cc = 0.0;
   	 ss = -1.0;
	}
	else
	{
   	 double miu = xx/fabs(xx);
    	 double tau = fabs(xx) + fabs(yy);
    	 double delta = tau*sqrt((xx/tau)*(xx/tau) + (yy/tau)* (yy/tau));
    	 cc = fabs(xx)/delta;
    	 ss = -miu*yy/delta;
	}

	*x = xx;
	*y = yy;
	*c = cc;
	*s = ss;
}

	
__device__ void SymMatrix2by2Eigen(double *a1, double *a2, double *b, double *c, double *s){

        double aa1 = *a1;
	double aa2 = *a2;
	double bb = *b;
	double cc = *c;
	double ss = *s;

	if (fabs(bb)<EPS)
	{
    	 cc = 1.0;
    	 ss = 0.0;
	}
	else if (fabs(aa1-aa2)<EPS)
	{
	 cc = sqrtf(2)/2;
   	 ss = cc;
	}
	else
	{
	 double theta = atan(2*bb/(aa1-aa2))/2;
    		if (bb<0)
       		 theta = theta + pi/2;
   	 ss = -sin(theta);
   	 cc = cos(theta);
	}

	*a1 = aa1;
	*a2 = aa2;
	*b = bb;
	*c = cc;
	*s = ss;

}

__device__ double norm2(int nn, double *v){
// 2-norm of a vector v
	double norm = 0;
	int i;
	for (i=0;i<nn;i++)
		norm = norm + v[i]*v[i];
	norm = sqrtf(norm);
	return norm;
}

__device__ void sort(int nn, double *a, int *sort){


    // the orginal sort is from 0:length(a)-1

    int i,j,s;
    double t;
    for(i=0;i<nn-1;++i)
    {                 
        for(j=0;j<nn-i-1;++j)
        {
            if(a[j]>a[j+1])
            {
                t = a[j];
                a[j] = a[j+1];
                a[j+1] = t;
		s = sort[j];
		sort[j] = sort[j+1];
		sort[j+1] = s;
            }
        }
    }
}

__device__ double rootfinding(int size_of_matrix, int number, double *D, double*v){

	int nn = size_of_matrix;
	double d_i = D[number];
	double d_i_1 = D[number + 1];
	double x = (d_i + d_i_1)/2;
	double vx = 1.0;
	int i,j;
	double de_i, de_i_1, a, b, c, p1, p2, pd1, pd2, eta;

	for (i=0;i<nn;i++)
		vx = vx + v[i]*v[i]/(D[i]-x);

	p1 = 0.0;
	p2 = 0.0;
	while (fabs(1+p1+p2)>0.0000001){
		de_i = d_i - x;
		de_i_1 = d_i_1 - x;
		p1 = 0.0; p2 = 0.0;
		pd1 = 0.0; pd2 = 0.0;
		for (j=0; j<(number+1);j++){
           		p1 = p1 + v[j]*v[j]/(D[j]-x);
          	  	pd1 = pd1 + v[j]*v[j]/((D[j]-x)*(D[j]-x));	
		}
		for (j=(number+1); j<nn; j++){
		        p2 = p2 + v[j]*v[j]/(D[j]-x);
        		pd2 = pd2 + v[j]*v[j]/((D[j]-x)*(D[j]-x));
		}
	        a = (1+p1+p2)*(de_i+de_i_1) - (pd1+pd2)*de_i*de_i_1;
       		b = (1+p1+p2)*de_i*de_i_1;
        	c = 1+p1+p2 - de_i*pd1 - de_i_1*pd2;
		eta = (a-sqrtf(a*a-4*b*c))/(2*c);
		printf("number = %d, eta = %g, 1+p1+p2 = %g\n",  number ,eta,  fabs(1+p1+p2));
		x = x+eta;
	}
return x;
}
	
__device__ void eigenvector(int size_of_matrix, int number, double *d, double *eigen, double *q){

	int nn = size_of_matrix;
	int k,j;
	double norm;

	for(k=0; k<nn; k++){
   		q[k] = 1;
	    	for (j=0; j<k; j++){
        		 q[k] = q[k]*(d[k]-eigen[j])/(d[k]-d[j]);
		}
    		q[k] = q[k]*(eigen[k]-d[k]);
    		for (j=k+1; j<nn; j++){
        		 q[k] = q[k]*(eigen[j]-d[k])/(d[j]-d[k]);
		}
    		q[k] = sqrtf(q[k]);
		q[k] = q[k]/(eigen[number] - d[k]);
	}

	norm = 0.0;
	for (k=0; k<nn; k++){
		norm = norm +  q[k]*q[k];
	}
	norm = sqrtf(norm);

	for (k=0; k<nn; k++){
		q[k] = q[k]/norm;
	}

}

__global__ void symmQR(double *a, double *b, double *Q){
	int m = n;
	int id = blockIdx.x;


	double shift, d, x, y, w, z, c, s;
	__shared__ double sa[n];
	__shared__ double sb[n-1];
	__shared__ double sQid[n][n];
	__shared__ double q1_new[n];
	__shared__ double q2_new[n];
  
     	double *Qid = Q + id*n*n;
	double *aid = a + id*n;
	double *bid = b + id*n;
	for (int i = 0; i < n; ++i){
		if (i<n-1){
		 sa[i] = aid[i];
		 sb[i] = bid[i];
		}
		else
		{
		 sa[i] =  aid[i]; //  - bid[i];
		}
		for (int j = 0; j < n; ++j)
	 	 sQid[j][i] = (i==j);
	}

	while (m>1){
		d = (sa[m-1-1] - sa[m-1])/2;
	    	if (d==0)
		{
	      	  shift = sa[m-1] - fabs(sb[m-1-1]);
		}
		else
		{
        	  shift = sa[m-1] - sb[m-1-1]*sb[m-1-1]/(d + fabs(d)/d * sqrtf(d*d + sb[m-1-1]*sb[m-1-1]));
		}
   		x = sa[1-1] - shift; // Implicit QR step begins here 
   		y = sb[1-1];
  		for (int k = 1; k < m; ++k){
       		 if (m>2)
           	    Givens(&x, &y, &c, &s);
		 else{
           	    SymMatrix2by2Eigen(&sa[1-1], &sa[2-1], &sb[1-1], &c, &s);
		 }
      		w = c*x - s*y;
        	d = sa[k-1] - sa[k+1-1];
	        z = (2*c*sb[k-1] + d*s)*s;
       		sa[k-1] = sa[k-1] - z;
        	sa[k+1-1] = sa[k+1-1] + z;
        	sb[k-1] = d*c*s + (c*c - s*s)* sb[k-1];
        	x = sb[k-1];
 	        if (k>1)
         	   sb[k-1-1] = w;
       		if (k<m-1)
		{
            	   y = -s*sb[k+1-1];
            	   sb[k+1-1] = c*sb[k+1-1];
		}
		for (int t=0; t<n; t++)
		{
			q1_new[t] = sQid[t][k-1]*c - sQid[t][k]*s;
			q2_new[t] = sQid[t][k-1]*s + sQid[t][k]*c;
			sQid[t][k-1] = q1_new[t];
			sQid[t][k] = q2_new[t];
		}
		} // Implicit QR step ends here
		if (fabs(sb[m-1-1])<0.000000001*(fabs(sa[m-1-1]) + fabs(sa[m-1])))
		{
       		 m = m-1;
		}        
	}

	for (int j=0; j<n; j++)
		for (int i=0; i<n; i++)
                Qid[j*n+i] = sQid[i][j];

	for (int t=0; t<n; t++){
		aid[t] = sa[t];}
}

#define s 64
#define NX 16 
#define NY 16

__global__ void assemble(double *a, double *b, double *Q, double *Q_temp, double *Q_eigen, double *Q_new, double * Q_out, const int  size_of_matrix){

        int id = blockIdx.x;
	int itx = threadIdx.x;
	int ity = threadIdx.y;
	int it = itx + NX*ity;
	int ord;

	int nn = size_of_matrix;
	double *bm = b + nn/2 - 1 + id*nn;
	double bmid = bm[0];

        __shared__ double sa[s];
        __shared__ double sd[s+1];
        __shared__ double sv[s];
        __shared__ double sv_order[s];
        __shared__ int  ssort[s];
        __shared__ double eigen[s];

        double *Qid_1 = Q + 2*id*nn*nn/4; //size(Q)= number_of_matrices(last step) * nn*nn/4
        double *Qid_2 = Q + (2*id + 1)*nn*nn/4;
	double *Qid_temp = Q_temp + id*nn*nn; // this matrix is a reordered Qid_out
	double *Qid_new = Q_new + id*nn*nn; // this is the true output eigenvector!
	double *eigenvec = Q_eigen + id*nn*nn; // this is eigenvector for D+vv'
      	double *aid = a + id*nn;
	double *Qid_out = Q_out + id*nn*nn; // size(Q) = number_of_matrices(this step) + nn*nn. This matrix = [Q1, 0; 0, Q2]


	// Only thread (0,0) is working

	if (it==0){
		// build Q = [Q1 0; 0 Q2];
		for (int i=0; i<nn; ++i){
			for (int j=0; j<nn; ++j){
				if((i<nn/2)&&(j<nn/2))
					Qid_out[j+i*nn] = Qid_1[j+i*nn/2];
				else if((i>nn/2-1)&&(j>nn/2-1))
					Qid_out[j+i*nn] = Qid_2[j-nn/2 + (i-nn/2) *nn/2];
				else
					Qid_out[j+i*nn] = 0.0;
			}
		}
#if 0
		printf("Qid_out = ");
		for(int i=0; i<nn; ++i){
			for (int j=0; j<nn-1; ++j){
				printf("%.10g ", Qid_out[i+j*nn]);
			}
			printf("%.10g \n", Qid_out[i+(nn-1)*nn]);
		}

#endif
		// build Diagonal and original order
    		for (int i = 0; i <nn; ++i){
        	         sa[i] = aid[i];
			 ssort[i] = i;
		}
		
		// build v
		double normv;
		for (int i = 0; i <nn/2; ++i){
       		         sv[i] = sqrtf(bmid) * Qid_1[nn/2-1 + nn/2*i]; // last row of Q1
			 sv[i+nn/2] = sqrtf(bmid) * Qid_2[nn/2*i]; //first row of Q2
       		}
		normv = norm2(nn, sv);
		
		if(id==-1){
			printf("\n sa before sort, id = %d, it = %d\n", id, it);
			for(int i=0; i<nn; ++i){
                        printf("%.10g\n ", sa[i]);
                }
		}

		// create sorted diagonal and new order
		sort(nn, sa, ssort);

		if(id==-1){
		printf("\nsa after sort\n");
		for(int i=0; i<nn; ++i){
                        printf("%.10g\n ", sa[i]);
                }
		printf("\nsort\n");
                for(int i=0; i<nn; ++i){
                        printf("%d\n ", ssort[i]);
                }}

		for  (int i = 0; i <nn; ++i){	
			sd[i] = sa[i];
		}
		sd[nn] = sd[nn-1] + 100.0*normv;
	//	for(int i=0; i<nn; ++i){
        //                printf("%.10g\n ", sd[i]);
        //        }

	}

	// printf("%d, %d\n", id ,it);
	
	__syncthreads();

	// reorder Q and v
	// Each thread reorder one column if nn<=256.
	if (nn<257){
		if (it<nn){
			ord = ssort[it];
			sv_order[it] = sv[ord];
			for(int i=0; i<nn; i++)
				Qid_temp[i + it*nn] = Qid_out[i + ord*nn];
		}
	}
	        if((id==-1)&&(it==0)){
                        printf("\n sv_order");
                        for(int i=0; i<nn; ++i){
                        printf("%.10g\n ", sv_order[i]);
                }
                }
#if 0
	if(it==0){
		printf("nn = %d\n", nn);
	        printf("sd = \n");
                for(int i=0; i<nn; ++i){
                                printf("%.10g\n ", sd[i]);
           	}
		printf("s_v = \n");
                for(int i=0; i<nn; ++i){
                                printf("%.10g\n", sv_order[i]);
                }
	}
#endif
#if 0
	else{
		int num = nn/257;
		for (int j=0;j<num; j++){
			new = ssort[it+j*256];
			sv_order[it + j*256] = sv[new];
			for (int i=0;i<nn; i++)
				Qid_temp[i +  (it+j*256) *nn] = Qid_out[i + new * nn];
		}
	}
#endif
	__syncthreads();

	// compute the root on the intervel [d_i, d_{i+1}] by it-th thread.
	if (nn<256){
		if (it<nn){
			eigen[it] = rootfinding(nn, it, sd, sv_order);
		}
	}

#if 0
	else{
		int num = nn/256;
		for (int j=0;j<num; j++){
			eigen[it + j*256] = rootfinding(nn, it+j*256, sd, sv_order);
		}
	}
#endif

	__syncthreads();

	// compute the eigenvector for D+vv'
	if (nn<256){
		if(it<nn){
			eigenvector(nn, it, sd, eigen, &(eigenvec[it*nn])); // --??
		}
	}


	 __syncthreads();
	// modify the sign of eigenvectors
	if(nn<1025){
		if(it<nn){
			if(sv_order[it]<0){
	//			printf("sv<0. it = %d\n", it);
				for (int j=0; j<nn; ++j){
					eigenvec[it + j*nn] =  - eigenvec[it+j*nn];
				}
			}
		}
	}
#if 0
	else{
		int num = nn/1024;
		for (int j=0;j<num; j++){
			eigenvector(nn, (it+j*1024), &sd, &eigen, &(eigenvec[(it+j*1024)*nn])); // --??
	}
	}
#endif
	__syncthreads();

	// multiply Qid_temp*eigenvector to get the output eigenvector
	if(nn<16){
		if((itx<nn)&&(ity<nn)){
			Qid_new[itx + ity*nn] = 0.0;
			for (int k=0; k<nn; k++){
				Qid_new[itx + ity*nn] = Qid_new[itx + ity*nn] + Qid_temp[itx + k*nn]* eigenvec[k +ity*nn];
			}
		}
	}
	else{
		int num = nn/16;
		for (int j=0;j<num; j++){
			for (int i=0;i<num; i++){
				int itxM = itx + j*16;
				int ityM = ity + i*16;
				Qid_new[itxM + ityM*nn] = 0.0;
				for (int k=0; k<nn; k++){
                 	               Qid_new[itxM + ityM*nn] = Qid_new[itxM + ityM*nn] + Qid_temp[itxM + k*nn]* eigenvec[k +ityM*nn];
                       		}
			}
		}
	}

	// only thread 0 copy sa back to aid
	if (it==0){
		for(int k=0; k<nn; k++){
			aid[k] = eigen[k];
		}
	}
	
}

int main(int argc, char **argv){
	cudaSetDevice(1); 
	if(argc<2){ printf("usage: ./DivedeAndConquer N\n"); exit(-1); }

        int power = atoi(argv[1]);
	int N = 1;
	for(int i=0; i<power; i++)
		N = 2*N;
	int half;
	int times;
	int Bqr = 1;
        int Gqr = N/n;


        double *h_a = (double*) malloc(N*sizeof(double));
	double *h_b = (double*) malloc((N-1)*sizeof(double));
	double *h_Q = (double*) malloc(N*N*sizeof(double));
	double *c_a, *c_b, *c_Q;
	double *Q_temp, *Q_eigen, *Q_new, *Q_out;

	cudaMalloc(&c_a, N*sizeof(double));
	cudaMalloc(&c_b, (N-1)*sizeof(double));
	cudaMalloc(&c_Q, N*N*sizeof(double));
	cudaMalloc(&Q_temp, N*N*sizeof(double));
	cudaMalloc(&Q_eigen, N*N*sizeof(double));
	cudaMalloc(&Q_new, N*N*sizeof(double));
	cudaMalloc(&Q_out, N*N*sizeof(double));

        for(int t = 0; t<N; ++t){
		 h_a[t] = t+1;
		 if (t<N-1)
			 h_b[t] = 15 +N-t;
	}

	half = N;
	times = 1;
	// convert h_a to a suitable vector
	for(int i=1; i<power-1; i++)
	{
		half = half/2;
		if (i>1)
			times = times*2;
		for (int j=0; j<times; j++){
			h_a[half-1+j*2*half] = h_a[half-1+j*2*half] - h_b[half-1 + j*2*half];
			h_a[half + j*2*half] = h_a[half + j*2*half] - h_b[half-1 + j*2*half];
		}
	}

	// QR

	cudaMemcpy(c_a, h_a, N*sizeof(double), cudaMemcpyHostToDevice);
  	cudaMemcpy(c_b, h_b, (N-1)*sizeof(double), cudaMemcpyHostToDevice);

	symmQR <<< Gqr , Bqr >>> (c_a, c_b, c_Q);

	cudaMemcpy(h_a, c_a, N*sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(h_Q, c_Q, N*N*sizeof(double), cudaMemcpyDeviceToHost);
	
#if 0
	printf("finish symmetric QR");
	for(int i=0;i<N;i++){
                printf(" h_a[i]=%g\n", h_a[i]);
        }
#endif
	// Assemble

	int Num_of_Assemble = power - 2; // If N=8, power=3, only need one assemble
	int size_of_matrix = n;
	for (int t=0; t<Num_of_Assemble; t++)
	{
		size_of_matrix = size_of_matrix*2;
		int G = N/size_of_matrix;
		dim3 B(NX,NY,1);
		// the actual size of c_Q should be G*size_of_matrix^2
		assemble <<< G, B >>> (c_a, c_b, c_Q, Q_temp, Q_eigen, Q_new, Q_out, size_of_matrix);
		printf("Size of matrix is %d\n", size_of_matrix);
		cudaMemcpy(h_Q, Q_new, N*N*sizeof(double), cudaMemcpyDeviceToHost);
       		cudaMemcpy(c_Q, h_Q, N*N*sizeof(double), cudaMemcpyHostToDevice);
	}	
 	cudaMemcpy(h_a, c_a, N*sizeof(double), cudaMemcpyDeviceToHost);

	printf("Size of the is matrix %d.\n", N);
	printf("\n");

	printf("The first eigenvector is \n");
        for(int i=0; i<N; ++i){
        //       for (int j=0; j<1; ++j){
        //                printf("%g  ", h_Q[i+j*N]);
        //                }
                        printf("%g\n", h_Q[i+(1-1)*N]);
        }
  	printf("\n");

	printf("The eigenvalues are\n");
	for(int i=0;i<N;i++){
		printf("%g\n", h_a[i]);
	}


	cudaFree(c_Q);
	cudaFree(c_a);
	cudaFree(c_b);
	cudaFree(Q_temp);
	cudaFree(Q_new);
	cudaFree(Q_out);
	free(h_Q);
	free(h_a);
	free(h_b);

return 0;
}




