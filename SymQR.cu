// n is the size of small matrices. n is defined in the begining.
// Input N is the number of matrices.


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "cuda.h"
#include <omp.h>

#define EPS 1e-10
#define pi 3.1415926
#define n 8

__device__ void Givens(double *x, double *y, double *c, double *s){

        double xx = *x;
        double yy = *y;
        double cc = *c;
        double ss = *s;

        if (fabs(yy)<EPS)
        {
         cc = 1.0;
         ss = 0.0;
        }
        else if (fabs(xx)<EPS)
        {
         cc = 0.0;
         ss = -1.0;
        }
        else
        {
         double miu = xx/fabs(xx);
         double tau = fabs(xx) + fabs(yy);
         double delta = tau*sqrt((xx/tau)*(xx/tau) + (yy/tau)* (yy/tau));
         cc = fabs(xx)/delta;
         ss = -miu*yy/delta;
        }

	*x = xx; *y = yy;
	*c = cc; *s = ss;

}


__device__ void SymMatrix2by2Eigen(double *a1, double *a2, double *b, double *c, double *s){

        double aa1 = *a1;
        double aa2 = *a2;
        double bb = *b;
        double cc = *c;
        double ss = *s;

        if (fabs(bb)<EPS)
        {
         cc = 1.0;
         ss = 0.0;
        }
        else if (fabs(aa1-aa2)<EPS)
        {
         cc = sqrtf(2)/2;
         ss = cc;
        }
        else
        {
         double theta = atan(2*bb/(aa1-aa2))/2;
                if (bb<0)
                 theta = theta + pi/2;
         ss = -sin(theta);
         cc = cos(theta);
        }

	*s = ss;
	*c = cc;
}



__global__ void symmQR(double *a, double *b, double *Q){
	
	int id = blockIdx.x;
	int idx = threadIdx.x;
        
	double shift, d, x, y, w, z;
	__shared__ double c,s;
	__shared__ int m;
        __shared__ double sa[n];
        __shared__ double sb[n-1];
        __shared__ double sQid[n][n];
        __shared__ double q1_new[n];
        __shared__ double q2_new[n];

	m = n;

        double *Qid = Q + id*n*n;
        double *aid = a + id*n;
        double *bid = b + id*n;
        
	if (idx<n-1){
              sa[idx] = aid[idx];
              sb[idx] = bid[idx];
                }
        else{
              sa[idx] =  aid[idx]; //  - bid[i];
        }
        
	for (int j = 0; j < n; ++j){
              sQid[j][idx] = (idx==j);
        }

	__syncthreads();

        while (m>1){
		if (idx==0) // only thread 0
		{	
                //	printf("%d, %d\n", idx, m);

			d = (sa[m-1-1] - sa[m-1])/2;
              		if (d==0)
                	{
                  	shift = sa[m-1] - fabs(sb[m-1-1]);
                	}
                	else
                	{
                  	shift = sa[m-1] - sb[m-1-1]*sb[m-1-1]/(d + fabs(d)/d * sqrtf(d*d + sb[m-1-1]*sb[m-1-1]));
                	}
                	x = sa[1-1] - shift; // Implicit QR step begins here
                	y = sb[1-1];
		}
                for (int k = 1; k < m; ++k){
			if (idx==0){ // only thread 0
                		if (m>2)
                    			Givens(&x, &y, &c, &s);
                 		else{
                    			SymMatrix2by2Eigen(&sa[1-1], &sa[2-1], &sb[1-1], &c, &s);
                 		}
               			w = c*x - s*y;
                		d = sa[k-1] - sa[k+1-1];
                		z = (2*c*sb[k-1] + d*s)*s;
                		sa[k-1] = sa[k-1] - z;
                		sa[k+1-1] = sa[k+1-1] + z;
                		sb[k-1] = d*c*s + (c*c - s*s)* sb[k-1];
                		x = sb[k-1];
                		if (k>1)
                   			sb[k-1-1] = w;
                		if (k<m-1)
                		{
                   			y = -s*sb[k+1-1];
                   			sb[k+1-1] = c*sb[k+1-1];
                		}
			}
                	 __syncthreads();
			//for (int t=0; t<n; t++)
               		{
                        q1_new[idx] = sQid[idx][k-1]*c - sQid[idx][k]*s;
                        q2_new[idx] = sQid[idx][k-1]*s + sQid[idx][k]*c;
                        sQid[idx][k-1] = q1_new[idx];
                        sQid[idx][k] = q2_new[idx];
                	}
                } // Implicit QR step ends here
		__syncthreads();
       		if (idx==0){ // only thread 0
			if (fabs(sb[m-1-1])<0.00000000001*(fabs(sa[m-1-1]) + fabs(sa[m-1]))) {
        		         m = m-1;
                	}
		}	
        	__syncthreads();
	}

        for (int i=0; i<n; i++)
                Qid[idx*n+i] = sQid[i][idx];
        aid[idx] = sa[idx];

}



int main(int argc, char **argv){

	cudaSetDevice(1);

	if(argc<2){ printf("usage: ./SymQR N\n"); exit(-1); }

  	int N = atoi(argv[1]);
	N = N*n;
        int Bqr = n;
        int Gqr = N/n;

        double *h_a = (double*) malloc(N*sizeof(double));
        double *h_b = (double*) malloc((N-1)*sizeof(double));
        double *h_Q = (double*) malloc(n*n*Gqr*sizeof(double));
        double *c_a, *c_b, *c_Q;
        cudaMalloc(&c_a, N*sizeof(double));
        cudaMalloc(&c_b, (N-1)*sizeof(double));
        cudaMalloc(&c_Q, n*n*Gqr*sizeof(double));

        for(int t = 0; t<N; ++t){
                 h_a[t] = t+1;
                 if (t<N-1)
                 h_b[t] = N-t;
        }

	// QR
        cudaMemcpy(c_a, h_a, N*sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(c_b, h_b, (N-1)*sizeof(double), cudaMemcpyHostToDevice);

        symmQR <<< Gqr , Bqr >>> (c_a, c_b, c_Q);

        cudaMemcpy(h_a, c_a, N*sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(h_Q, c_Q, Gqr*n*n*sizeof(double), cudaMemcpyDeviceToHost);

	for (int i=0;i<N/n;i++){
		 printf("---Eigenvalues for the %d-th matrix are \n ", 1+i);
		for (int j=0;j<n-1;j++){
       			printf("%g, ", h_a[j+n*i]);}
		printf("%g\n ", h_a[n-1+n*i]);
		 printf("---Eigenvectors for the %d-th matrix are \n", 1+i);
		for (int j=0;j<n;j++){
			for (int k=0;k<n-1;k++){
                        	printf("%g, ", h_Q[j + n*k + n*n*i]);
			}
			printf("%g\n", h_Q[j + n*(n-1) + n*n*i]);
		}
	}


	cudaFree(c_Q);
  	cudaFree(c_a);
  	cudaFree(c_b);
  	free(h_Q);
  	free(h_a);
  	free(h_b);

  return 0;
}


